(async () => {
    const sleep = time => new Promise(resolve => setTimeout(resolve, time));
    let prevScrollY = 0;
    const resultsMap = {};
    const extractCells = (cells, type, commandsArr, commandsObj) => {
        const bets = {};
        const total = {};
        cells.forEach((cell, i) => {
            switch(i) {
                case 0:
                    bets[commandsArr[0]] = cell.textContent;
                    break;
                case 1:
                    bets.draw = cell.textContent;
                    break;
                case 2:
                    bets[commandsArr[2]] = cell.textContent;
                    break;
                case 3:
                    total.less = cell.textContent;
                    break;
                case 4:
                    total.total = cell.textContent;
                    break;
                case 5:
                    total.more = cell.textContent;
                    break;
            }
        });
        commandsObj[type] = {
            bets,
            total
        };
    };
    while(document.scrollingElement.scrollHeight !== prevScrollY) {
        Array.from(document.getElementsByClassName('table__item')).forEach((tableItem) => {
            const commandsArr = Array.from(tableItem.querySelectorAll('.statistic__match .statistic__team span'))
                .map(span => span.textContent);
            const commands = commandsArr.join(' ');
            if(resultsMap.hasOwnProperty(commands)) {
                return;
            }
            const commandsObj = {};
            resultsMap[commands] = commandsObj;
            let type = tableItem.querySelector('.statistic__match .icon__text').textContent;
            let cells = tableItem.querySelectorAll('.coefficient__cell .coefficient__td');
            extractCells(cells, type, commandsArr, commandsObj);

            if(!tableItem.querySelector('.statistic.statistic--second-string .icon__text')) {
                return;
            }
            type = tableItem.querySelector('.statistic.statistic--second-string .icon__text').textContent;
            cells = tableItem.querySelectorAll('.coefficient.coefficient--second-string .coefficient__td');
            extractCells(cells, type, commandsArr, commandsObj);
        });


        prevScrollY = document.scrollingElement.scrollHeight;
        window.scrollTo(0, document.scrollingElement.scrollHeight);
        await sleep(100);
    }

    console.log(resultsMap);
})();
